package com.matritel.recipebook.service;


import com.matritel.recipebook.domain.RecipeDto;
import com.matritel.recipebook.repository.RecipeBookRepository;
import com.matritel.recipebook.repository.entity.RecipeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RecipeBookService {

    @Autowired
    private RecipeBookRepository recipeBookRepository;

    public List<RecipeDto> retrieveAllRecipes() {
        List<RecipeEntity> entityList = recipeBookRepository.findAll();
        List<RecipeDto> dtoList = new ArrayList<>();
        for (RecipeEntity recipeEntity : entityList
        ) {
            dtoList.add(toDto(recipeEntity));
        }
        return dtoList;
    }

    public Optional<RecipeDto> retrieveRecipe(String id) {
        Optional<RecipeEntity> entity = recipeBookRepository.findById(id);
        if (entity.isPresent()) {
            return Optional.of(toDto(entity.get()));
        } else
            return Optional.empty();
    }

    public void deleteRecipeById(String id) {
        recipeBookRepository.deleteById(id);
    }

    public List<RecipeDto> retrieveAllByCreatorName(String name) {
        List<RecipeEntity> entityList = recipeBookRepository.findAllByCreatorName(name);
        List<RecipeDto> dtoList = new ArrayList<>();
        for (RecipeEntity recipeEntity : entityList
        ) {
            dtoList.add(toDto(recipeEntity));
        }
        return dtoList;
    }

    public RecipeDto create(RecipeDto recipeDto) {
        RecipeEntity toSave = toEntity(recipeDto);
        RecipeEntity saved = recipeBookRepository.save(toSave);
        return toDto(saved);
    }

    private RecipeEntity toEntity(RecipeDto recipeDto) {
        return RecipeEntity.builder()
                .creatorName(recipeDto.getCreatorName())
                .date(LocalDate.now())
                .instructions(recipeDto.getInstructions())
                .title(recipeDto.getTitle())
                .ingredients(recipeDto.getIngredients())
                .build();
    }

    private RecipeDto toDto(RecipeEntity recipeEntity) {
        return RecipeDto.builder()
                .creatorName(recipeEntity.getCreatorName())
                .date(recipeEntity.getDate())
                .id(recipeEntity.getId())
                .ingredients(recipeEntity.getIngredients())
                .instructions(recipeEntity.getInstructions())
                .title(recipeEntity.getTitle())
                .build();
    }
}
