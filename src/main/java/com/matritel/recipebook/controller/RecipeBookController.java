package com.matritel.recipebook.controller;

import com.matritel.recipebook.domain.RecipeDto;
import com.matritel.recipebook.service.RecipeBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/matritel/recipebook")
public class RecipeBookController {

    @Autowired
    private RecipeBookService recipeBookService;

    @PostMapping(value = "/recipes")
    @ResponseStatus(HttpStatus.CREATED)
    public RecipeDto postRecipe(@Valid @RequestBody RecipeDto recipeDto) {
        return recipeBookService.create(recipeDto);
    }

    @GetMapping(value = "/recipes")
    @ResponseStatus(HttpStatus.OK)
    public List<RecipeDto> getAllRecipes() {
        return recipeBookService.retrieveAllRecipes();
    }

    @GetMapping(value = "/recipes/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RecipeDto getRecipe(@PathVariable String id) {
        if (recipeBookService.retrieveRecipe(id).isPresent()) {
            return recipeBookService.retrieveRecipe(id).get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "not found");
    }

    @DeleteMapping(value = "/recipes/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRecipe(@PathVariable String id) {
        recipeBookService.deleteRecipeById(id);
    }

    @GetMapping(value = "/recipes/findByCreator")
    @ResponseStatus(HttpStatus.OK)
    public List<RecipeDto> getRecipesByCreator(@RequestParam(name = "creatorName") String name) {
        return recipeBookService.retrieveAllByCreatorName(name);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        return new ErrorResponse("Wrong request: " + ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return new ErrorResponse("wrong argument: " + ex.getMessage());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse mapMethodAEntityNotFoundException(EntityNotFoundException ex) {
        return new ErrorResponse("wrong argument: " + ex.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse mapExecption(Exception ex) {
        return new ErrorResponse("Internal Server Error: " + ex.getMessage());
    }
}
