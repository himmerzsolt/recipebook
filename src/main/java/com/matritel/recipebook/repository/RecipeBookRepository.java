package com.matritel.recipebook.repository;

import com.matritel.recipebook.repository.entity.RecipeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RecipeBookRepository extends CrudRepository<RecipeEntity, String> {
    List<RecipeEntity> findAll();

    List<RecipeEntity> findAllByCreatorName(String name);
}
