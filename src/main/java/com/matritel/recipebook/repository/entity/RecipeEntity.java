package com.matritel.recipebook.repository.entity;

import com.matritel.recipebook.domain.Ingredient;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity(name = "recipebook")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class RecipeEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @Column(name = "title", columnDefinition = "VARCHAR(80)")
    private String title;
    @Column(name = "instructions", columnDefinition = "VARCHAR(255)")
    private String instructions;
    @ElementCollection
    @Embedded
    private List<Ingredient> ingredients;
    @Column(name = "creator_name", columnDefinition = "VARCHAR(20)")
    private String creatorName;
    @Column(name = "created_at")
    private LocalDate date;

}
