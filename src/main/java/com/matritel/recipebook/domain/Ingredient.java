package com.matritel.recipebook.domain;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
@Embeddable
public class Ingredient {

    @NotNull
    @Embedded
    private Measurement measurement;
    @NotNull
    @Size(max = 20)
    private String name;
}
