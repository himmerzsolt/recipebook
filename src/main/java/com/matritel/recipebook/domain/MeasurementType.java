package com.matritel.recipebook.domain;

public enum MeasurementType {
    GRAMS, MILLILITERS
}
