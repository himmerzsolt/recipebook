package com.matritel.recipebook.domain;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
class Measurement {
    @NotNull
    @Size(max = 10)
    private int amount;
    @NotNull
    @Enumerated(EnumType.STRING)
    private MeasurementType type;

}
