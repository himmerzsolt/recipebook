package com.matritel.recipebook.domain;


import lombok.*;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RecipeDto {

    private String id;
    @NotNull
    @Size(max = 80)
    private String title;
    @NotNull
    @Size(max = 255)
    private String instructions;
    @NotNull
    @Embedded
    @ElementCollection
    private List<Ingredient> ingredients;
    @NotNull
    @Size(max = 20)
    private String creatorName;
    private LocalDate date;
}
